#ifndef RAPTOR_APPLICATIONS_PLUGIN_H
#define RAPTOR_APPLICATIONS_PLUGIN_H

#include "../raptorlet.h"
#include <KGenericFactory>

class infor
{
public:
	infor(){}
	~infor(){}
};


class RaptorApplicationsPlugin:public Raptorlet
{
/*	
    Q_OBJECT
//	Q_INTERFACES(RaptorInterface)
public: 
	~RaptorApplicationsPlugin();
        RaptorInterface::Dict search (const QString & key);	
        RaptorInterface::Dict data ();
*/
    Q_OBJECT
    public:
        RaptorApplicationsPlugin(QObject * parent,const QStringList &args );
        ~RaptorApplicationsPlugin();

};

//K_EXPORT_COMPONENT_FACTORY(raptorappplugin, KGenericFactory<RaptorApplicationsPlugin> )
K_EXPORT_RAPTORLET(apps,RaptorApplicationsPlugin) 

#endif 
