#include "raptorsliderviewgroup.h"
#include <QTimer>
#include <plasma/phase.h>

class RaptorSliderViewGroup::Private
{
  public:
    Private(){}
    ~Private(){}
    stackHash slideLayer;
    int slideCount;
    RaptorSlide * currentLayer;
    int currentViewId;
    QTimer * slideTimer;
    RaptorSlide::Direction mode;
    int m_max;
    int itemPerRow;
};

RaptorSliderViewGroup::RaptorSliderViewGroup(QGraphicsScene * parent):QObject(),d(new Private)
{
  d->currentLayer = new RaptorSlide();
  d->mode = RaptorSlide::Grid;
  d->currentLayer->setDirection(d->mode);
  d->currentLayer->move(10.0,0.0);
  d->slideCount = 0;
  d->slideLayer[0] = d->currentLayer;
  canvas = parent;
  d->currentViewId=0;
  d->slideTimer = new QTimer(this);
  d->m_max= 2;
  d->itemPerRow = 2;
}

RaptorSliderViewGroup::~RaptorSliderViewGroup()
{
  delete d;
  delete canvas;
}
void RaptorSliderViewGroup::setMode(RaptorSlide::Direction m)
{
    d->mode = m;
     for (int i = 0; i < d->slideLayer.count(); i++) {
             // d->slideLayer[i]->move(x,y);
                  d->slideLayer[i]->setDirection(d->mode);
               }
}
bool RaptorSliderViewGroup::addItem(RaptorClaw * item)
{
    
    if (!d->currentLayer->isFull()) {
       
        d->currentLayer->addItem((RaptorClaw*)item);
        d->currentLayer->setDirection(d->mode);
        canvas->addItem(item);
        item->hide();
    } else {
        d->currentLayer = new RaptorSlide();
        d->currentLayer->setItemLimit(d->m_max);
        d->currentLayer->setItemPerRow(d->itemPerRow);
        d->currentLayer->setDirection(d->mode);
        d->slideLayer[++d->slideCount] = d->currentLayer;
        d->currentLayer->addItem((RaptorClaw*)item);
        canvas->addItem(item);
        item->hide();
    }

}

bool RaptorSliderViewGroup::deleteItem(QGraphicsItem * item)
{
}

QGraphicsItem * RaptorSliderViewGroup::find (const QString& key)
{
}

int RaptorSliderViewGroup::slideCount()
{
    return d->slideCount;
}

void RaptorSliderViewGroup::view(int id)
{
    //we want to hide quick for now.. 
    //TODO : add phase animation integration.here
 for (int i = 0; i < d->slideLayer.count(); i++) {
   //  Plasma::Phase().moveItem(d->slideLayer[i],Plasma::Phase::SlideOut,QPoint(400,400));
     d->slideLayer[i]->hide();
 }
    if (d->slideLayer[id]) {
                 d->slideLayer[id]->show();
    }
    d->currentViewId= id;
}
//protected

void RaptorSliderViewGroup::swapNext()
{
    if ((d->currentViewId  ) < d->slideCount)
    {
            d->currentViewId = d->currentViewId + 1;
            this->view(d->currentViewId);
    }
    qDebug("Now Swaping");
}

void RaptorSliderViewGroup::swapPrevious()
{
    if ( d->currentViewId -1 >= 0) {
        this->view(--d->currentViewId);
    }
}

void RaptorSliderViewGroup::move(float x , float y)
{
     for (int i = 0; i < d->slideLayer.count(); i++) {
              d->slideLayer[i]->move(x,y);
               }
     
}

float RaptorSliderViewGroup::height()
{
   return  d->currentLayer->height();
}

float RaptorSliderViewGroup::width()
{
   return  d->currentLayer->width();
}

QRectF RaptorSliderViewGroup::boundingRect() const
{
    return QRectF(0,0,d->currentLayer->width(),d->currentLayer->height());
}

void RaptorSliderViewGroup::setItemLimit(int limit)
{
    d->m_max=limit;
 for (int i = 0; i < d->slideLayer.count(); i++) {
     d->slideLayer[i]->setItemLimit(limit);

 }

}

void RaptorSliderViewGroup::setItemPerRow(int limit)
{
    d->itemPerRow = limit;
 for (int i = 0; i < d->slideLayer.count(); i++) {
     d->slideLayer[i]->setItemPerRow(limit);
    }
}


#include "raptorsliderviewgroup.moc"
