
#ifndef FAKE_PLASMA_H
#define FAKE_PLASMA_H
#include <QGraphicsView>

class FakePlasma : public QGraphicsView
{
    public:
    FakePlasma (QWidget * parent = 0);
    ~FakePlasma();
};
#endif

