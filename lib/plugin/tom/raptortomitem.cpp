#include "raptortomitem.h"


class RaptorTomItem::Private
{
    public:
        Private(){}
        ~Private(){}
	QString name;
	QString iconPath;
	QString iconName;

};

RaptorTomItem::RaptorTomItem(QObject * parent):RaptorDataSource(parent),d(new Private){
}

RaptorTomItem::~RaptorTomItem(){
}

QString RaptorTomItem::name(){

	return d->name;
}

void RaptorTomItem::setName(const QString & name){

	d->name = name;
}

QString RaptorTomItem::iconPath(){

	return d->iconPath;
}

void RaptorTomItem::setIconPath(const QString & path){

	d->iconPath = path;
}

QString RaptorTomItem::iconName(){

	return d->iconName;
}
	
void RaptorTomItem::setIconName(const QString & name){

	d->iconName = name;
}

void RaptorTomItem::exec(){

	//Do exec
}

bool RaptorTomItem::lookup(const QString & key){

	//Do lookup
}

#include "raptortomitem.moc"

