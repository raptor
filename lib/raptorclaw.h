//GPL2 siraj@kde.org


#ifndef RAPTOR_CLAW_H
#define RAPTOR_CLAW_H

#include <QGraphicsItem>
#include <QtCore/QObject>
#include <QtGui/QGraphicsTextItem>
#include <QtGui/QLayoutItem>
//plasma
#include <plasma/svg.h>
#include <plasma/theme.h>
#include <plasma/datacontainer.h>
#include "raptorlet.h"
#include "plasmicraptor_export.h"

/**
 * A raptor claw is a item onthe slider view
 * it has the most basic properties that defines a
 * slide 
 **/

class PLASMICRAPTOR_EXPORT RaptorClaw : public QObject,
                public QGraphicsItem,
		public QLayoutItem
{
Q_OBJECT
    public:

    typedef enum { REGULAR,OVER,PRESSED } MouseState;
    typedef QHash <MouseState,QString> ThemeNames;

    RaptorClaw(QGraphicsItem * parent = 0);
    virtual ~RaptorClaw();
   
   
    void paint( QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget * widget = 0);
    
    
    QSize sizeHint() const;
    QSize minimumSize() const; 
    QSize maximumSize() const ;
    QRectF boundingRect() const;

    Qt::Orientations expandingDirections() const{return Qt::Horizontal;}

    QRect geometry() const;
    void setGeometry(const QRect&) {}
    
    bool isEmpty() const {return false;}
    
    QString name();
    void setName(const QString& name);
    
    void setOpacity(float op);


    void setIcon(QPixmap icon);

    public slots:

    protected:
	virtual void hoverEnterEvent ( QGraphicsSceneHoverEvent * event );
	virtual void hoverMoveEvent ( QGraphicsSceneHoverEvent * event );
	virtual void hoverLeaveEvent ( QGraphicsSceneHoverEvent * event );

    private:
    QString loadSvg(MouseState state);
    class Private;
    Private * const d ;

};

#endif
