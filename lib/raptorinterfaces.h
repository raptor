//GPL2  : Siraj Razick : siraj@kdemail.net

#ifndef RAPTOR_INTERFACE_H
#define RAPTOR_INTERFACE_H

#include <QString>
#include <QtPlugin>
#include <QHash>

//raptor

#include "raptordatasource.h"

/**
Raptorlets should inherit this abstract interface 
class to implement compatible plugins for raptor
***/

class RaptorInterface
{
	public:
		typedef QHash <QString,RaptorDataSource*> Dict;

		virtual ~RaptorInterface() {}

		virtual Dict search (const QString & key) = 0;
		
		virtual Dict   data () = 0;
		
};

Q_DECLARE_INTERFACE(RaptorInterface, "org.kde.plasma.RaptorInterface/1.0")

#endif
