#include "element.h"
#include <QPainter>
#include <KIcon>
#include <kicontheme.h>
#include "element.moc"

class Element::Private
{
    public:
        Private(){}
        ~Private(){}
        int height;
        int width;
        Plasma::Svg * m_theme;
        QString text;
        QString comment;
        QString tooltip;
	QString icon;	
        int Rating;
	MouseState state;
	ThemeNames ids;
        QSize size;
        QSize elementSize;
        QPixmap iconPixmap;
        float opacity;
};


Element::Element(QGraphicsItem * parent) : QGraphicsItem(parent),d(new Private)
{
//TODO
setAcceptsHoverEvents(true);
    //read from config 
d->height = 330;
d->width = 310;
d->m_theme = new Plasma::Svg( "menu/raptorslide",this);
d->m_theme->resize(d->width,d->height);

d->text = "RaptorItem";
d->ids[REGULAR] = "svgbg";
d->ids[OVER] = QString("svgbg");
d->ids[PRESSED] = "svgbg";
d->state = REGULAR;
d->size = d->m_theme->size();
d->elementSize = d->m_theme->elementSize("backgruond");
d->iconPixmap = QPixmap();
d->opacity = 1.0f;
}

Element::~Element()
{
delete d->m_theme;
delete d;
}

void Element::paint ( QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget * widget )
{

    painter->setOpacity(d->opacity);
    painter->setRenderHint(QPainter::SmoothPixmapTransform);
    d->m_theme->resize(d->width,d->height);
    QRectF source(x(), y(), d->elementSize.width() , d->elementSize.height());
    d->m_theme->paint(painter, x(),y(), d->ids[d->state]);
    if (!d->iconPixmap.isNull()) {
        painter->drawPixmap(x()+((int)source.width()-d->iconPixmap.width())/2,y()+ 
                ((int)source.height()-d->iconPixmap.height())/2,d->iconPixmap.height(),d->iconPixmap.width(),d->iconPixmap);
    } else {
    }

    //painter->drawText(source,Qt::AlignCenter,d->text); 
}

void Element::setOpacity(float op)
{
    d->opacity = op;
}
QString Element::loadSvg(MouseState state)
{
	return d->ids[state];
}

QSize Element::sizeHint() const
{
return QSize(137,38);
}

QRectF Element::boundingRect() const
{
    return QRectF (x(),y(),d->elementSize.width(),d->elementSize.height());
}
//Events

void Element::hoverEnterEvent ( QGraphicsSceneHoverEvent * event )
{
	d->state = OVER;
	update();
}

void Element::hoverMoveEvent ( QGraphicsSceneHoverEvent * event )
{
	d->state = OVER;
	update();
}

void Element::hoverLeaveEvent ( QGraphicsSceneHoverEvent * event )
{
	d->state = REGULAR;
	update();
}

QString Element::name()
{
return     d->text;
}

void Element::setName(const QString& name)
{
    d->text = name;
}

void Element::setIcon(QPixmap icon)
{
    kDebug()<<icon.height();
    d->iconPixmap = QPixmap(icon);
}
