
#ifndef RAPTORMENUWIDGET_H
#define RAPTORMENUWIDGET_H
#include <QtCore>
#include <QtGui>
#include <QAbstractScrollArea>
#include <QMouseEvent>

#include "fakeplasma.h"
#include "raptorslide.h"
#include "../lib/frontend/raptorclaw.h"
//#include "element.h"
#include "../lib/raptorpluginloader.h"
#include "../lib/raptorlet.h"
#include "../lib/raptordatasourcegroup.h"
#include "raptorsliderviewgroup.h"
#include "raptorscrollhandler.h"
#include "raptormenuwidget.h"
#include "raptorcanvas.h"


class RaptorMenuWidget : public QGraphicsView
{
    Q_OBJECT

public:
    RaptorMenuWidget();

    void itemMoved();

protected:
    void keyPressEvent(QKeyEvent *event);
    void timerEvent(QTimerEvent *event);
    void wheelEvent(QWheelEvent *event);
    void drawBackground(QPainter *painter, const QRectF &rect);
    void scaleView(qreal scaleFactor);
    void loadDefaultPlugin();

    //

public slots:
    void update(const QRectF& rect);
    void loadView(); //for now empty
    //void animate(int step);
    //void animate();
    //void finishAnimation();
    void ajustView ( const QRectF & rect ) ;
    void updateSceneRect ( const QRectF & rect );
protected:
    void setSelfProperties();
private:
    class Private;
    Private * const d;


///FIXME: Move to private class
    int timerId;
    QPoint clickPos;
    bool clicked;
    QTimeLine timeline;
    QTimer * timer;
    int angle;
};

#endif
