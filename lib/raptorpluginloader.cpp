#include "raptorpluginloader.h"
//Qt
#include <QDir>
#include <QPluginLoader>
//KDE4
#include <KStandardDirs>
#include <kdebug.h>
#include <KService>
#include <KServiceTypeTrader>

class RaptorPluginLoader::Private
{
	public :
//		Private();
		QString mBasePath;
                QStringList  * PluginList;
                bool mode;
};

RaptorPluginLoader::RaptorPluginLoader(QObject * parent) : QObject(parent)
{
	this->d = new Private();
        d->PluginList = new QStringList();
        d->mode = scan();
}

RaptorPluginLoader::~RaptorPluginLoader()
{
	delete d;
}

bool RaptorPluginLoader::scan()
{
    QStringList raptorlets;
    Raptorlet * raptorlet;
    KService::List offers = KServiceTypeTrader::self()->query("Raptor/Raptorlet");

    foreach (KService::Ptr service , offers) {
    kDebug()<<service->property("Name").toString()<<endl;
    d->PluginList->append(service->property("Name").toString());
    pluginMap() [ service->property("Name").toString() ] =
        KService::createInstance<Raptorlet>(service, 0);
    }
    if ( offers.count() > 0) {
        return true;
    }
    return false;
}

QStringList * RaptorPluginLoader::pluginList()
{
    return d->PluginList;
}

RaptorPluginLoader::PluginMap&
RaptorPluginLoader::pluginMap()
{
    static PluginMap  * map = 0;
    if ( !map ) {
        map = new PluginMap();
    }

    return *map;
}

Raptorlet * RaptorPluginLoader::getDataService(const QString & name)
{

    if ( d->PluginList->contains(name)) {
        Raptorlet * raptorlet = pluginMap()[name];
          if (raptorlet ) {
            return raptorlet;
          }
    }
    return NULL;
}



#include "raptorpluginloader.moc"
