
#include "raptorcanvas.h"

#include <QDebug>
#include <QGraphicsScene>
#include <QWheelEvent>


#include <math.h>

RaptorCanvas::RaptorCanvas(QObject * parent )
    : timerId(0),QGraphicsScene(parent)
{

}

void RaptorCanvas::itemMoved()
{
}

void RaptorCanvas::keyPressEvent(QKeyEvent *event)
{
}

void RaptorCanvas::timerEvent(QTimerEvent *event)
{
    Q_UNUSED(event);

}

void RaptorCanvas::wheelEvent(QWheelEvent *event)
{
}

void RaptorCanvas::drawBackground(QPainter *painter, const QRectF &rect)
{
    
      painter->setCompositionMode(QPainter::CompositionMode_Source);

      painter->fillRect(QRect(rect.x()-2,rect.y()-2,rect.width()+2,rect.height()+2), Qt::transparent);
  
}


#include "raptorcanvas.moc"
