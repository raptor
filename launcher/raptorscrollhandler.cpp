#include "raptorscrollhandler.h"
#include <QPainter>
#include "raptorscrollhandler.moc"

class RaptorScrollHandler::Private
{
    public:
        Private(){}
        ~Private(){}
        int height;
        int width;
        Plasma::Svg * m_theme;
        QString comment;
        QString tooltip;
	MouseState state;
	ThemeNames ids;
        QSize size;
        QSize elementSize;
        Qt::Orientations orientation;
        ArrowState arrow;
};


RaptorScrollHandler::RaptorScrollHandler(QGraphicsItem * parent) : QGraphicsItem(parent),d(new Private)
{
//TODO
setAcceptsHoverEvents(true);
    //read from config 
d->height = 240;
d->width = 340;
d->m_theme = new Plasma::Svg( "menu/raptorslide",this);
d->m_theme->resize(d->height,d->width);
//TODO don't do this!
setExpandingDirections(Qt::Vertical);
d->state = REGULAR;
d->size = d->m_theme->size();
//d->size  = QSize(d->size.width()/10,d->size.height()/10);
d->elementSize = d->m_theme->elementSize("arrowUp");
d->arrow = UP;
d->orientation = Qt::Vertical;
}

void RaptorScrollHandler::setArrowState(RaptorScrollHandler::ArrowState state)
{
    if (state == UP) {
         d->ids[REGULAR] = "arrowUp";
         d->ids[OVER] = "arrowUp";
         d->ids[PRESSED] = "arrowUp";
    } else if ( state == DOWN) {
         d->ids[REGULAR] = "arrowDown";
         d->ids[OVER] = "arrowDown";
         d->ids[PRESSED] = "arrowDown";
    } else if ( state == LEFT) {
         d->ids[REGULAR] = "arrowLeft";
         d->ids[OVER] = "arrowLeft";
         d->ids[PRESSED] = "arrowLeft";
         d->elementSize = d->m_theme->elementSize("arrowLeft");
    } else if ( state == RIGHT) {
         d->ids[REGULAR] = "arrowRight";
         d->ids[OVER] = "arrowRight";
         d->ids[PRESSED] = "arrowRight";
         d->elementSize = d->m_theme->elementSize("arrowRight");
    }
    d->arrow = state;
}


RaptorScrollHandler::~RaptorScrollHandler()
{
delete d->m_theme;
delete d;
}

void RaptorScrollHandler::setExpandingDirections(Qt::Orientations ori)
{
 /*
    d->orientation = ori;
    if (d->orientation == Qt::Vertical) {
        d->ids[REGULAR] = "arrowUp";
        d->ids[OVER] = "arrowUp";
        d->ids[PRESSED] = "arrowUp";
           
    } else {
        d->ids[REGULAR] = "arrowBot";
        d->ids[OVER] = "arrowBot";
        d->ids[PRESSED] = "arrowDown";

    }
    */
}

void RaptorScrollHandler::paint ( QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget * widget )
{
    d->m_theme->resize(d->height,d->width);
    painter->setRenderHint(QPainter::SmoothPixmapTransform);
    QRectF source = boundingRect();//(x(), y(), d->width , d->height );
    d->m_theme->paint(painter, x(),y(), d->ids[d->state]);
    //painter->drawRect(boundingRect());
}

QString RaptorScrollHandler::loadSvg(MouseState state)
{
	return d->ids[state];
}

QSize RaptorScrollHandler::sizeHint() const
{
return QSize(d->width,d->height);
}

QRectF RaptorScrollHandler::boundingRect() const
{
    //kDebug() << QRectF (x(),y(),d->elementSize.width(),d->elementSize.height());
    return QRectF (x(),y(),d->elementSize.width(),d->elementSize.height());
}

/*
QRect RaptorScrollHandler::geometry () const
{
    return QRect (x(),y(),d->width,d->height);
}

*/
//Events

void RaptorScrollHandler::hoverEnterEvent ( QGraphicsSceneHoverEvent * event )
{
	d->state = OVER;
        emit activated();
	update();
}

void RaptorScrollHandler::hoverMoveEvent ( QGraphicsSceneHoverEvent * event )
{
    
       d->m_theme->resize(d->width,d->height);
	d->state = OVER;
	update();
}

void RaptorScrollHandler::hoverLeaveEvent ( QGraphicsSceneHoverEvent * event )
{
	d->state = REGULAR;
	update();
}

void RaptorScrollHandler::mousePressEvent ( QGraphicsSceneMouseEvent * event )
{
    d->state = OVER;
         emit activated();
       update();

}
