#include "raptorclaw.h"
#include <QPainter>
#include <KIcon>
#include <kicontheme.h>
#include <QPainterPath>
#include <QtGui>
#include <QtCore>
#include "raptorclaw.moc"

class RaptorClaw::Private
{
    public:
        Private(){}
        ~Private(){}
        int height;
        int width;
        Plasma::Svg * m_theme;
        QString text;
        QString comment;
        QString tooltip;
	QString icon;	
        int Rating;
	MouseState state;
	ThemeNames ids;
        QSize size;
        QSize elementSize;
        QPixmap iconPixmap;
        float opacity;
        //Grid
        int ncol;
        int nrow;
        QTimeLine timeline;
};


RaptorClaw::RaptorClaw(QGraphicsItem * parent) : QGraphicsItem(parent),d(new Private)
{
//TODO
setAcceptsHoverEvents(true);
    //read from config 
d->height = 330;
d->width = 340;
d->m_theme = new Plasma::Svg( "menu/raptorslide",this);
d->m_theme->resize(d->width,d->height);
//FIXME
//Do not hardcode Get from config/xml
d->text = "RaptorItem";
d->ids[REGULAR] = "itemNormal";
d->ids[OVER] = QString("itemOver");
d->ids[PRESSED] = "itemOver";
d->state = REGULAR;
d->size = d->m_theme->size();
d->elementSize = d->m_theme->elementSize("itemNormal");
d->iconPixmap = QPixmap();
d->opacity = 1.0f;
//FIXME: need methods for these
d->nrow=4;
d->ncol=4;
//simple animation
d->timeline.setDuration(400);
d->timeline.setFrameRange(0,20);
//d->timeline.setUpdateInterval(1000/100);
connect(&d->timeline,SIGNAL(frameChanged(int)),this,SLOT(zoom(int)));


}

RaptorClaw::~RaptorClaw()
{
delete d->m_theme;
delete d;
}

void RaptorClaw::paint ( QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget * widget )
{
    Q_UNUSED(widget);
    Q_UNUSED(option);
     QRectF rect = option->exposedRect; 
     
    painter->setOpacity(d->opacity);
    painter->setRenderHint(QPainter::SmoothPixmapTransform);
    d->m_theme->resize(d->width,d->height);
    QRectF source(0,0 , d->elementSize.width() , d->elementSize.height());
    d->m_theme->paint(painter, 0,0, d->ids[d->state]);
    if (!d->iconPixmap.isNull()) {
        painter->drawPixmap(int(((int)source.width()-d->iconPixmap.width())/2),int( 
                ((int)source.height()-d->iconPixmap.height())/2),d->iconPixmap.height(),d->iconPixmap.width(),d->iconPixmap);
    }

    painter->setRenderHint(QPainter::Antialiasing,true);
    QRectF textRect(2,(-6)+d->iconPixmap.height()+((int)source.height()-d->iconPixmap.height())/2,d->elementSize.width()-4 ,
            painter->fontMetrics().height() );
if ( d->state == OVER){
    QPainterPath textPath;
    textPath.addRoundRect(textRect,75);
    painter->setPen(QPen(QColor(0,0,0),2,Qt::SolidLine,Qt::RoundCap,Qt::RoundJoin));
    painter->setOpacity(0.7);

    //FIXME
   //remove hand coded values
    QLinearGradient fillgrad(0,0,0,310);
    fillgrad.setColorAt(0.0,QColor(255,70,0));
    fillgrad.setColorAt(1.0,QColor(237,255,149));
    painter->setBrush(fillgrad);
    painter->drawPath(textPath);
    painter->setPen(QColor(255,255,255)); 
    painter->setRenderHint(QPainter::TextAntialiasing);
    painter->drawText(textRect,Qt::AlignCenter,painter->fontMetrics().elidedText(d->text,Qt::ElideRight,d->elementSize.width()-8)); 
}
    painter->setPen(QColor(255,255,255)); 
    painter->drawText(textRect,Qt::AlignCenter,painter->fontMetrics().elidedText(d->text,Qt::ElideRight,d->elementSize.width()-8)); 
}

void RaptorClaw::setOpacity(float op)
{
    d->opacity = op;
}
QString RaptorClaw::loadSvg(MouseState state)
{
	return d->ids[state];
}

QSize RaptorClaw::sizeHint() const
{
return QSize(d->height,d->width);
}

QSize RaptorClaw::minimumSize() const
{
    return QSize(d->height,d->width);
}

QRect RaptorClaw::geometry() const 
{
    return QRect(0,0,d->height,d->width);
}

QSize RaptorClaw::maximumSize() const
{
    return QSize(d->height,d->width);
}
QRectF RaptorClaw::boundingRect() const
{
    return QRectF (0,0,d->elementSize.width(),d->elementSize.height());
}
//Events

void RaptorClaw::hoverEnterEvent ( QGraphicsSceneHoverEvent * event )
{
        Q_UNUSED(event);
	d->state = OVER;
	update();
        d->timeline.setDirection(QTimeLine::Forward);
        if (d->timeline.state() == QTimeLine::NotRunning)
        d->timeline.start();
        emit clicked();
}

void RaptorClaw::hoverMoveEvent ( QGraphicsSceneHoverEvent * event )
{
        Q_UNUSED(event);
	d->state = OVER;
	update();

}

void RaptorClaw::hoverLeaveEvent ( QGraphicsSceneHoverEvent * event )
{
        Q_UNUSED(event);
	d->state = REGULAR;
	update();
        d->timeline.setDirection(QTimeLine::Backward);
        if (d->timeline.state() == QTimeLine::NotRunning)
          d->timeline.start();

}

QString RaptorClaw::name()
{
return     d->text;
}

void RaptorClaw::setName(const QString& name)
{
    d->text = name;
}

void RaptorClaw::setIcon(QPixmap icon)
{
    d->iconPixmap = QPixmap(icon);
}

void RaptorClaw::zoom(int step)
{
    
    QPointF center = this->boundingRect().center();
//    resetTransform();
    resetMatrix();
    QTransform mat = this->transform();
    mat.translate(center.x(),center.y());
    mat.scale(1+step/150.0,1+step/150.00);
    mat.translate(-center.x(),-center.y());

    this->setTransform(mat);

}
