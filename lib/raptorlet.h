#ifndef RAPTORLET_H
#define RAPTORLET_H

#include <QObject>
#include "plasmicraptor_export.h"
#include "raptordatasourcegroup.h"

class PLASMICRAPTOR_EXPORT Raptorlet : public QObject
{
    Q_OBJECT
    public:
        typedef enum { STATIC,STREAMING,ONUPDATE} EngineMode; 
        Raptorlet(QObject * parent=0);
        ~Raptorlet();
        virtual QString name() =0;
        virtual RaptorDataSourceGroup * data() =0;
        virtual RaptorDataSourceGroup * search (const QString& key) = 0;
};

#define K_EXPORT_RAPTORLET(libname, classname) \
            K_EXPORT_COMPONENT_FACTORY(                \
                            raptorlet_##libname,    \
                                            KGenericFactory<classname>("raptorlet_" #libname))
#endif

