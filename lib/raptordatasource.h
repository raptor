
/**
This class will provide a compatible interface to
datasource but hey this is the menu, we need to simplify 
DataSource is atomic and indivisible , This defines the unit
**/

#ifndef RAPTOR_DATA_SOURCE_H
#define RAPTOR_DATA_SOURCE_H

#include <QtCore>
#include <QObject>
#include <QVector>

#include "plasmicraptor_export.h"

class PLASMICRAPTOR_EXPORT RaptorDataSource : public QObject
{

Q_OBJECT 

public:
       // typedef QMap <QString,RaptorDataSource* > DataSourceList;
	RaptorDataSource(QObject * parent = 0);
	virtual ~RaptorDataSource();

	/*
	*/
	virtual	QString name() = 0 ;
	
	/*
	*/

	virtual void setName(const QString & name) = 0 ;

	/*
	should we store the whole path or just the name ?...hmmm
	*/

	virtual QString iconPath() = 0 ;

	/*
	*/

	virtual	void setIconPath(const QString & path) = 0;

	/*
	*/

	virtual QString iconName() = 0 ;
	
	/*
	*/

	virtual	void  setIconName(const QString & name) = 0 ;	
	/*
	*/

	virtual void exec() = 0;

        /*
           */
        virtual bool lookup(const QString & key) = 0;
};


#endif


