
#include "raptormenuwidget.h"

#include <QDebug>
#include <QGraphicsScene>
#include <QWheelEvent>
#include <kaboutdata.h>
#include <kcmdlineargs.h>
#include <klocale.h>
#include <KIcon>
#include <QGraphicsScene>
#include <kicontheme.h>
#include <kservicegroup.h>
#include <kservice.h>

//plsma
#include <QGraphicsRectItem>
#include <plasma/widgets/pushbutton.h>
#include <plasma/widgets/icon.h>

//raptor
#include "bgelement.h"
#include <math.h>

class RaptorMenuWidget::Private
{
    public:
        Private(){};
        ~Private(){};
    
        QRectF boundingrect;
        RaptorCanvas *scene;
        QTimeLine timeline;
        QTimer * timer;
   //     QTimeLine viewAnimLine;
        int angle;
        Plasma::Svg * background;
        RaptorSliderViewGroup * view;
        RaptorSliderViewGroup * viewindex;
        BgElement * e;
};

RaptorMenuWidget::RaptorMenuWidget()
    : timerId(0),clicked(0),d(new Private)
{ 
    /*View Properties*/
    setSelfProperties();
    
    /* Basic UI items*/
    d->view = new RaptorSliderViewGroup(d->scene);
    d->viewindex = new RaptorSliderViewGroup(d->scene);

    d->view->setItemLimit(4);
    d->view->setItemPerRow(2);
    d->boundingrect = this->sceneRect();
    d->e->setSize(sceneRect().width(),sceneRect().height());
    resize(sceneRect().width()+40,sceneRect().height()+60);
;
    d->viewindex->setItemLimit(4);
    d->viewindex->setItemPerRow(4);

    d->view->setMode(RaptorSlide::Grid);
    d->viewindex->setMode(RaptorSlide::Horizontal);

    /*Setup UI properties*/
    /* load data */
    loadDefaultPlugin();

    d->view->view(0);
    d->viewindex->view(0);
    //TODO
    ///read menu size

    d->boundingrect =
    QRectF(0,0,
            d->viewindex->getDefaultScene()->sceneRect().width()+20,
            d->view->getDefaultScene()->sceneRect().height() +
            d->viewindex->getDefaultScene()->sceneRect().height());
   /* Signal connections*/
    connect(d->scene,SIGNAL(sceneRectChanged(const QRectF &)),this,SLOT(update(const QRectF&)));

  //Plasma settup
   d->background = new Plasma::Svg( "widgets/background",this);

   //size 
   d->background->resize((int)d->view->getDefaultScene()->sceneRect
           ().width(),(int)d->view->getDefaultScene()->sceneRect ().height());

    move(10,10);

   //move
   loadView();
   d->view->move(25,20);
   d->viewindex->move(25,(d->view->height()*2)+50);



    d->boundingrect = this->sceneRect();
    d->e->setSize(sceneRect().width(),sceneRect().height());
    resize(sceneRect().width()+60,sceneRect().height()+60);
    //animation;

}


void RaptorMenuWidget::setSelfProperties()
{
    setWindowFlags(Qt::FramelessWindowHint);
    d->scene = new RaptorCanvas();
    d->scene->setItemIndexMethod(QGraphicsScene::NoIndex);
    setScene(d->scene);
    setRenderHint(QPainter::Antialiasing);
    setTransformationAnchor(AnchorUnderMouse);
    setResizeAnchor(AnchorUnderMouse);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setLineWidth(0);
    setFrameStyle(QFrame::NoFrame);
    centerOn(QPointF(0.0,0.0));
    setAlignment(Qt::AlignLeft|Qt::AlignTop);
    // setViewportUpdateMode(QGraphicsView::FullViewportUpdate);
       d->e  = new BgElement();
    d->e->show();
    d->e->setPos(10,10);
    d->scene->addItem(d->e);

}

void RaptorMenuWidget::itemMoved()
{
    if (!timerId)
        timerId = startTimer(1000 / 25);
}

void RaptorMenuWidget::keyPressEvent(QKeyEvent *e)
{
    if (e->key() == Qt::Key_Left) {

    }
    if (e->key() == Qt::Key_Up) {
     
    }
    if ( e->key() == Qt::Key_W) {
    timeline.stop();
    QPointF center = d->boundingrect.center();
    resetMatrix();
    QTransform mat = transform();
    mat.translate(center.x(),center.y());
   // mat.rotate(step,Qt::XAxis);
    mat.rotate(-45,Qt::YAxis);
    mat.translate(-center.x(),-center.y());
    setTransform(mat);
    }
    if ( e->key() == Qt::Key_Q) {
    
    QPointF center = d->boundingrect.center();
    resetMatrix();
    QTransform mat = transform();
    mat.translate(center.x(),center.y());
    mat.rotate(360,Qt::YAxis);
    mat.translate(-center.x(),-center.y());
    setTransform(mat);

    }

}

void RaptorMenuWidget::timerEvent(QTimerEvent *event)
{
    Q_UNUSED(event);

}

void RaptorMenuWidget::wheelEvent(QWheelEvent *event)
{
    scaleView(pow((double)2, -event->delta() / 240.0));
}

void RaptorMenuWidget::drawBackground(QPainter *painter, const QRectF &rect)
{

   Q_UNUSED(rect);
      painter->setCompositionMode(QPainter::CompositionMode_Source);
      painter->fillRect(QRect((int)rect.x(),(int)rect.y(),(int)rect.width()+2,(int)rect.height()+2), Qt::transparent);

      painter->setCompositionMode(QPainter::CompositionMode_SourceOver);

}

void RaptorMenuWidget::scaleView(qreal scaleFactor)
{
    qreal factor = matrix().scale(scaleFactor, scaleFactor).mapRect(QRectF(QCursor::pos().x(), 0, 1, 1)).width();
    if (factor < 0.07 || factor > 100)
    {
            return;
    }
    scale(scaleFactor, scaleFactor);

}

void RaptorMenuWidget::update(const QRectF& rect)
{
    Q_UNUSED(rect);
}

void RaptorMenuWidget::loadDefaultPlugin()
{
   RaptorPluginLoader * loader = new RaptorPluginLoader();
    for (int i = 0; i < loader->pluginList()->size(); ++i) {
    RaptorClaw * item = new RaptorClaw();
    Raptorlet * rcore= loader->getDataService(loader->pluginList()->at(i));
    if ( rcore != NULL)
    item->setName(rcore->name());
    item->setIcon(KIcon ("kmenu").pixmap(64,64));
    d->viewindex->addItem(item);
    
    RaptorDataSourceGroup  * datagroup  = rcore->data();
        if (datagroup == NULL) {
        kDebug()<<"Data Group is null"<<endl;
        continue;
        }

        RaptorDataSourceGroup::Data datampa = datagroup->getData();

        ///kDebug()<<"+-============> data source count "<< datagroup->getData().count() <<endl;

        
    connect(item,SIGNAL(clicked()),this,SLOT(loadView()));

    }
}

void RaptorMenuWidget::loadView()
{
    for ( int i = 0 ; i < 16 ; i++)
{
    RaptorClaw * item = new RaptorClaw();
    item->setName(QString("Item %1").arg(i));
    item->setIcon(KIcon ("book").pixmap(64,64));
    d->view->addItem(item);
    d->view->view(0);
}

}

void RaptorMenuWidget::ajustView ( const QRectF & rect ) 
{
    kDebug()<<rect<<endl;
}

void RaptorMenuWidget::updateSceneRect ( const QRectF & rect )
{
    /*
    d->boundingrect = this->sceneRect();
    d->e->setSize(sceneRect().width(),sceneRect().height());
    resize(sceneRect().width()+40,sceneRect().height()+60);
*/
}
#include "raptormenuwidget.moc"
