//GPL 2 siraj@kde.org

#ifndef RAPTOR_CLAW_ANIMATOR_H
#define RAPTOR_CLAW_ANIMATOR_H

#include <QtCore>
#include <QtGui>
#include "raptorclaw.h"
#include "../plasmicraptor_export.h"

class PLASMICRAPTOR_EXPORT RaptorClawAimator : public QGraphicsItemAnimation
{
    Q_OBJECT
public:
    typedef enum  {IN,OUT,IDLE} Animation ;
    RaptorClawAimator(RaptorClaw * claw,Animation type = IDLE);
    ~RaptorClawAimator();
    virtual void play(bool start=true);
    /*
    virtual void stop();
    virtual void rewind();
    bool isAnimating();
*/
    public slots:
        void finished();
        void update(int);
private:
    class Private;
    Private * const d ;

};
#endif
