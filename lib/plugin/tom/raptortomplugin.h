#ifndef RAPTOR_TOM_PLUGIN_H
#define RAPTOR_TOM_PLUGIN_H

#include "../../raptorlet.h"
#include <KGenericFactory>

class infor
{
public:
	infor(){}
	~infor(){}
};


class RaptorTomPlugin:public Raptorlet
{
    Q_OBJECT
    public:
        RaptorTomPlugin(QObject * parent,const QStringList &args );
        ~RaptorTomPlugin();
        QString name() { return QString("Tasks");}
        RaptorDataSourceGroup * data();
        RaptorDataSourceGroup * search (const QString& key);

};

K_EXPORT_RAPTORLET(tom,RaptorTomPlugin) 

#endif 
