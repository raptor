//GPL2 siraj@kde.org


#ifndef RAPTOR_SCROLL_HANDLER_H
#define RAPTOR_SCROLL_HANDLER_H

#include <QGraphicsItem>
#include <QtCore/QObject>
#include <QtGui/QGraphicsTextItem>
#include <QtGui/QLayoutItem>
//plasma
#include <plasma/svg.h>
#include <plasma/theme.h>
#include <plasma/datacontainer.h>


class RaptorScrollHandler : public QObject,
                public QGraphicsItem,
		public QLayoutItem
{
Q_OBJECT
    public:
    typedef enum { REGULAR,OVER,PRESSED } MouseState;
    typedef QHash <MouseState,QString> ThemeNames;
    typedef enum {UP=0,DOWN,LEFT,RIGHT} ArrowState;

    RaptorScrollHandler(QGraphicsItem * parent = 0);

    virtual ~RaptorScrollHandler();

    QRectF boundingRect() const;
    void paint( QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget * widget = 0);
    QSize sizeHint() const;//{}
    //FIXME:
    QSize minimumSize() const {return QSize(100,100);}
    QSize maximumSize() const {return QSize(100,100);}
    void setArrowState(RaptorScrollHandler::ArrowState state);
    Qt::Orientations expandingDirections() const{return 0;}
    void setExpandingDirections(Qt::Orientations ori);
    void setGeometry(const QRect& r) {Q_UNUSED(r)}
    QRect geometry() const {return QRect(0,0,100,100);};
    bool isEmpty() const { return false;}

    public slots:
 
     signals:
        void activated();
    protected:
	virtual void hoverEnterEvent ( QGraphicsSceneHoverEvent * event );
	virtual void hoverMoveEvent ( QGraphicsSceneHoverEvent * event );
	virtual void hoverLeaveEvent ( QGraphicsSceneHoverEvent * event );
        virtual void mousePressEvent ( QGraphicsSceneMouseEvent * event );
    private:
    QString loadSvg(MouseState state);
    class Private;
    Private * const d ;

};

#endif
