/*
 *   Copyright (C) 2006
 *   Siraj Razick <siraj@kdemail.net>
 *   see Also AUTHORS
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License version 2 as
 *   published by the Free Software Foundation
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "raptordatasourcegroup.h"

RaptorDataSourceGroup::RaptorDataSourceGroup()
{
	m_name = "Uknown Group";
	m_count = 0;
}

RaptorDataSourceGroup::~RaptorDataSourceGroup()
{
	Data::Iterator it;
	for ( it = m_data.begin();it!=m_data.end();++it )
	{
		//delete it.data();
	//	m_data.remove ( it );
		//delete *it;
                //
	}
	m_data.clear();
}

RaptorDataSourceGroup::Data
RaptorDataSourceGroup::lookup ( QString str )
{
//static RaptorDataSourceGroup * _matching   = new RaptorDataSourceGroup();
	RaptorDataSourceGroup::Data _matching;
	Data::Iterator it;
	for ( it = m_data.begin();it!=m_data.end();++it )
	{
		if ( ( *it )->lookup ( str ) == true )
		{
		//	_matching.prepend ( * ( *it ) );
                    _matching[(*it)->name()] = *it;
		}
	}

	return _matching;
}

void
RaptorDataSourceGroup::addItem ( RaptorDataSource * item )
{
	if ( item ==  0 )
		return ;
	m_data[item->name() ] = item;
	m_index[m_count] = item;
	m_count++;
	
//	qDebug("RaptorDataSourceGroup.cpp::addItem("+item->name()+")");
}

RaptorDataSource* 
RaptorDataSourceGroup::itemAt(int index)
{
	if ( index < 0 || index > m_count)
	return 0;
	else
	return m_index[index];
}


RaptorDataSource *
RaptorDataSourceGroup::getItem ( QString name )
{
	return m_data[name];
}

RaptorDataSourceGroup::Data
RaptorDataSourceGroup::getData()
{
	return m_data;
}

