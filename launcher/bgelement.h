//GPL2 siraj@kde.org


#ifndef BG_ELEMENT_H
#define BG_ELEMENT_H

#include <QGraphicsItem>
#include <QtCore>
#include <QtGui>

//plasma
#include <plasma/svg.h>
#include <plasma/theme.h>
#include <plasma/datacontainer.h>
//raptor
#include "element.h"
/**
 * A raptor claw is a item onthe slider view
 * it has the most basic properties that defines a
 * slide 
 **/

class BgElement : public QObject , public QGraphicsItem
{
Q_OBJECT
    public:
    typedef enum { REGULAR,OVER,PRESSED } MouseState;
    typedef QHash <MouseState,QString> ThemeNames;

    BgElement(QGraphicsItem * parent = 0);
    virtual ~BgElement();
    void setSize(int width,int height);
    QRectF boundingRect() const;
    void paint( QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget * widget = 0);
    QSizeF sizeHint() const;//{}
    public slots:
   
    protected:
	virtual void hoverEnterEvent ( QGraphicsSceneHoverEvent * event );
	virtual void hoverMoveEvent ( QGraphicsSceneHoverEvent * event );
	virtual void hoverLeaveEvent ( QGraphicsSceneHoverEvent * event );

    private:
    class Private;
    Private * const d ;

};

#endif
