
#ifndef RAPTORCANVAS_H
#define RAPTORCANVAS_H

#include <QGraphicsView>
#include <QGraphicsScene>
//plasma
#include <plasma/svg.h>
#include <plasma/theme.h>
#include <plasma/datacontainer.h>



class RaptorCanvas : public QGraphicsScene 
{
    Q_OBJECT

public:
    RaptorCanvas(QObject * parent = 0);

    void itemMoved();

protected:
    void keyPressEvent(QKeyEvent *event);
    void timerEvent(QTimerEvent *event);
    void wheelEvent(QWheelEvent *event);
    void drawBackground(QPainter *painter, const QRectF &rect);


private:
    int timerId;
};

#endif
