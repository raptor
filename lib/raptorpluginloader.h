#ifndef RAPTOR_PLUGIN_LOADER_H
#define RAPTOR_PLUGIN_LOADER_H

#include <QObject>
#include <QLibrary>
#include <QMap>
#include "raptorlet.h"
#include "plasmicraptor_export.h"

class PLASMICRAPTOR_EXPORT RaptorPluginLoader : public QObject
{
Q_OBJECT

	public:
                typedef QMap <QString,Raptorlet*> PluginMap;

		RaptorPluginLoader(QObject * parent = 0);
		virtual ~RaptorPluginLoader();
		void setBasePath(const QString & path);
		bool scan();
                Raptorlet * getDataService(const QString & name);
                QStringList* pluginList();

signals:
		void pluginReady(int id);
		
	private	:
		class Private ;
		Private *  d;		
                static  PluginMap& pluginMap();

};

#endif
