#ifndef RAPTOR_APPLICATIONS_PLUGIN_H
#define RAPTOR_APPLICATIONS_PLUGIN_H

#include "../../raptorlet.h"
#include <KGenericFactory>
#include <KLocalizedString>

class infor
{
public:
	infor(){}
	~infor(){}
};


class RaptorApplicationsPlugin:public Raptorlet
{
    Q_OBJECT
    public:
        RaptorApplicationsPlugin(QObject * parent,const QStringList &args );
        ~RaptorApplicationsPlugin();
        QString name() { return QString(i18n("Applications"));}
        RaptorDataSourceGroup * data();
        RaptorDataSourceGroup * search (const QString& key);

};

//K_EXPORT_COMPONENT_FACTORY(raptorappplugin, KGenericFactory<RaptorApplicationsPlugin> )
K_EXPORT_RAPTORLET(apps,RaptorApplicationsPlugin) 

#endif 
