#include <QWidget>
#include <QApplication>


//KDE4
#include <kapplication.h>
#include <kaboutdata.h>
#include <kcmdlineargs.h>
#include <klocale.h>
#include <KIcon>
#include <QGraphicsScene>
#include <kicontheme.h>
#include <kservicegroup.h>
#include <kservice.h>
//testers
#include "fakeplasma.h"
#include "raptorslide.h"
//#include "element.h"
#include "../lib/raptorpluginloader.h"
#include "raptorsliderviewgroup.h"
#include "raptorscrollhandler.h"
#include "raptormenuwidget.h"

//plsma
#include <QGraphicsRectItem>
#include <plasma/widgets/pushbutton.h>
#include <plasma/widgets/icon.h>
#include "qtdisplay.h"

static const char description[] =
    I18N_NOOP("A KDE4 Plasma Sub-Project ");

static const char version[] = "%{VERSION}";



int main(int argc,char * argv [])
{
#ifdef QT43_ONLY
QApplication app(argc,argv);
return app.exec();
#endif


KAboutData about("Raptor", 0, ki18n("Raptor"), version, ki18n(description),
                     KAboutData::License_GPL, ki18n("(C) 2007 Siraj Razick}"), KLocalizedString(), 0, "siraj@kde.org");
    about.addAuthor( ki18n("Siraj Razick"), KLocalizedString(), "siraj@kde.org" );
    about.addAuthor( ki18n("Thomas Lübking"), KLocalizedString(), "baghira-style@users.sf.net" );
    about.addAuthor( ki18n("Aaron Seigo"), KLocalizedString(), "aseigo@kde.org" );
    //TODO:
    //find who is Mlaurent and his email 
    ///  about.addAuthor( ki18n("Mlaurent"),KLocalizedString(),"dirk@kde.org" );
    about.addAuthor( ki18n("Phobos Kappa"), KLocalizedString(), "phobosk@kdemail.net" );
    about.addAuthor( ki18n("Ephrics"), KLocalizedString(), ".@." );
    KCmdLineArgs::init(argc, argv, &about);

    KCmdLineOptions options;
    options.add("None", ki18n( "No additional parameters" ));
    KCmdLineArgs::addCmdLineOptions(options);
    QtDisplay * dpy = new QtDisplay();
 KApplication app(dpy->display(),dpy->visual(),dpy->colormap());
//    KApplication app;


    RaptorMenuWidget * menu = new RaptorMenuWidget(); 
    menu->show();
    menu->move(100,100);
  
    return app.exec();
}

