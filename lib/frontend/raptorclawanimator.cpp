#include "raptorclawanimator.h"

class RaptorClawAimator::Private
{
    public:
        Private(){}
        ~Private(){}
        QTimeLine * timeline;
        RaptorClaw * claw;
        Animation type;
        QPointF itemPos;
        bool curruption;
        int i;

};

RaptorClawAimator::RaptorClawAimator(RaptorClaw * claw,Animation type )
    : d(new Private)
{
    d->claw = claw;
    d->type = type;
    d->timeline = new QTimeLine(200);
    d->timeline->setFrameRange(0,100);
    d->timeline->setUpdateInterval(160);
    d->timeline->setDuration(100);
    setTimeLine(d->timeline);
    if (claw) {
        setItem(d->claw);
        d->curruption = false;
    } else {
        d->curruption = true;
    }
    //demo
    for (int i = 0; i < 200 ; i++) {
        this->setPosAt(i/200,QPointF(0,0));
    }
    d->itemPos = d->claw->pos();
    connect(d->timeline,SIGNAL(finished()) ,this,SLOT(finished()));
    connect(d->timeline,SIGNAL(frameChanged(int)) ,this,SLOT(update(int)));
    d->i = 0;
}

RaptorClawAimator::~RaptorClawAimator()
{
    delete d->timeline;
}

void RaptorClawAimator::play(bool start)
{
    if (d->curruption == true) {
        qDebug("Invalid Animator Calling Play()");
        return;
    }
    if (d->claw->isVisible() == false) {
        qDebug("Hidden Item");
        d->timeline->stop();
        return;
    }


    d->timeline->stop();
    d->timeline->setCurrentTime(0);
    d->timeline->setStartFrame(0);
    QPointF current = d->claw->pos();
    setPosAt(0.0,d->itemPos);
    if (start){ 
    d->timeline->start();
    qDebug("Starting Timer");
    }
}

void RaptorClawAimator::finished()
{
    d->claw->setPos(d->itemPos);
    d->claw->hide();
}

void RaptorClawAimator::update(int frm)
{
    Q_UNUSED(frm);
    d->claw->moveBy(0,10);
}
