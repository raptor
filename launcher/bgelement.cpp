#include "bgelement.h"
#include <QPainter>
#include <KIcon>
#include <kicontheme.h>
#include "bgelement.moc"

class BgElement::Private
{
    public:
        Private(){}
        ~Private(){}
        int height;
        int width;
        Plasma::Svg * background;
        QString text;
        QString comment;
        QString tooltip;
	QString icon;	
        int Rating;
	MouseState state;
	ThemeNames ids;
        QSize size;
        QSize elementSize;
        QPixmap iconPixmap;
        float opacity;
        int contentHeight;
        int contentWidth;
        QRectF boundingrect;
};


BgElement::BgElement(QGraphicsItem * parent) : QGraphicsItem(parent),d(new Private)
{
//TODO
setAcceptsHoverEvents(true);
    //read from config 
d->height = 330;
d->width = 310;
d->background = new Plasma::Svg( "widgets/background",this);
d->background->resize(d->width,d->height);

d->text = "RaptorItem";
d->ids[REGULAR] = "svgbg";
d->ids[OVER] = QString("svgbg");
d->ids[PRESSED] = "svgbg";
d->state = REGULAR;
d->size = d->background->size();
d->elementSize = d->background->elementSize("center");
d->iconPixmap = QPixmap();
d->opacity = 1.0f;
d->contentWidth=400;
d->contentHeight=400;

}

BgElement::~BgElement()
{
delete d->background;
delete d;
}

void BgElement::paint ( QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget * widget )
{
    d->boundingrect = boundingRect();
    d->background->resize();
        const int topHeight = d->background->elementSize("top").height();
        const int topWidth = d->background->elementSize("top").width();
        const int leftWidth = d->background->elementSize("left").width();
        const int leftHeight = d->background->elementSize("left").height();
        const int rightWidth = d->background->elementSize("right").width();
        const int bottomHeight = d->background->elementSize("bottom").height();

        const int topOffset = 0- topHeight;
        const int leftOffset = 0 - leftWidth;
        const int rightOffset = (int) d->boundingrect.width();
        const int bottomOffset = (int) d->boundingrect.height();
        const int contentTop =0 ;
        const int contentLeft = 0;
        painter->setRenderHint(QPainter::SmoothPixmapTransform);

        painter->translate(leftWidth, topHeight); 

            d->background->paint(painter, QRect(leftOffset,  topOffset,    leftWidth,  topHeight),    "topleft");
            d->background->paint(painter, QRect(rightOffset, topOffset,    rightWidth, topHeight),    "topright");
            d->background->paint(painter, QRect(leftOffset,  bottomOffset, leftWidth,  bottomHeight), "bottomleft");
            d->background->paint(painter, QRect(rightOffset, bottomOffset, rightWidth, bottomHeight), "bottomright");
        QPixmap left(leftWidth, leftHeight);

            left.fill(Qt::transparent);
            {
                QPainter sidePainter(&left);
                sidePainter.setCompositionMode(QPainter::CompositionMode_Source);
                d->background->paint(&sidePainter, QPoint(0, 0), "left");
            }
            painter->drawTiledPixmap(QRect(leftOffset, contentTop, leftWidth,(int)d->boundingrect.height()), left);

            QPixmap right(rightWidth, leftHeight);
            right.fill(Qt::transparent);
            {
                QPainter sidePainter(&right);
                sidePainter.setCompositionMode(QPainter::CompositionMode_Source);
                d->background->paint(&sidePainter, QPoint(0, 0), "right");
            }
            painter->drawTiledPixmap(QRect(rightOffset, contentTop, rightWidth,
                       (int) d->boundingrect.height()), right);

            QPixmap top(topWidth, topHeight);
            top.fill(Qt::transparent);
            {
                QPainter sidePainter(&top);
                sidePainter.setCompositionMode(QPainter::CompositionMode_Source);
                d->background->paint(&sidePainter, QPoint(0, 0), "top");
            }
            painter->drawTiledPixmap(QRect(contentLeft, topOffset,(int) d->boundingrect.width(), topHeight), top);
    QPixmap bottom(topWidth, bottomHeight);
            bottom.fill(Qt::transparent);
            {
                QPainter sidePainter(&bottom);
                sidePainter.setCompositionMode(QPainter::CompositionMode_Source);
                d->background->paint(&sidePainter, QPoint(0, 0), "bottom");
            }
            painter->drawTiledPixmap(QRect(contentLeft,
                        bottomOffset,(int)d->boundingrect.width(), bottomHeight), bottom);
            /*
            d->background->paint(painter, QRect(contentLeft, contentTop, (int)boundingrect.width() + 1,
                        (int)boundingrect.height() + 1),"center");
            */
           // painter->setOpacity(0.2);
            painter->fillRect(QRect(contentLeft, contentTop, (int)d->boundingrect.width() + 1,
                                                (int)d->boundingrect.height() + 1),QColor(0,0,0));

}






QSizeF BgElement::sizeHint() const
{
return boundingRect().size();
}

void BgElement::setSize(int w, int h)
{
    d->contentWidth=w;
    d->contentHeight=h ;
}

QRectF BgElement::boundingRect() const
{
    return QRectF (0,0,d->contentWidth,d->contentHeight);
}
//Events

void BgElement::hoverEnterEvent ( QGraphicsSceneHoverEvent * event )
{
	d->state = OVER;
	update();
}

void BgElement::hoverMoveEvent ( QGraphicsSceneHoverEvent * event )
{
	d->state = OVER;
	update();
}

void BgElement::hoverLeaveEvent ( QGraphicsSceneHoverEvent * event )
{
	d->state = REGULAR;
	update();
}


