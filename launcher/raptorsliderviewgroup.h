//GPL2 cod : siraj@kde.org

#ifndef RAPTOR_SLIDER_VIEW_GROUP_H
#define RAPTOR_SLIDER_VIEW_GROUP_H

#include <QGraphicsItemGroup>
#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QHash>
#include "raptorslide.h"
#include "../lib/frontend/raptorclaw.h"
#include "raptorscrollhandler.h"


class RaptorSliderViewGroup :public QObject,public QGraphicsItemGroup
{
  Q_OBJECT
 public:
	typedef QHash <int,RaptorSlide*> stackHash;
	
        RaptorSliderViewGroup( QGraphicsScene * part = new QGraphicsScene() );
	virtual ~RaptorSliderViewGroup();
        QGraphicsScene * getDefaultScene() { return canvas;}
	bool addItem(RaptorClaw *);
	bool deleteItem(QGraphicsItem*);
	QGraphicsItem * find(const QString&);
        void setMode(RaptorSlide::Direction);
        int slideCount();
        void view(int id);
        void move(float x , float y);
        float height();
        float width();
        QRectF  boundingRect() const;
        void setItemLimit(int);
	void setItemPerRow(int);

        public slots:
            void swapNext();
            void swapPrevious();
            //TODO
            //swap by Name :
            //Swap by Integer ID

 //protected:
//        void addScrolls();
        
  private:
	class Private;
	Private * const d;
        QGraphicsScene * canvas;
};

#endif
