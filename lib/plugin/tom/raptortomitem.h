#ifndef RAPTOR_TOM_ITEM_H
#define RAPTOR_TOM_ITEM_H

#include <QVector>
#include <QMap>
#include <QtCore>
#include "../../raptordatasource.h"

class RaptorTomItem : public RaptorDataSource
{

Q_OBJECT

public:
	RaptorTomItem(QObject * parent = 0);
        ~RaptorTomItem();
        /*
	*/
	QString name();
	
	/*
	*/

	 void setName(const QString & name);

	/*
	should we store the whole path or just the name ?...hmmm
	*/

	 QString iconPath();

	/*
	*/

		void setIconPath(const QString & path) ;

	/*
	*/

	 QString iconName();
	
	/*
	*/

		void  setIconName(const QString & name);	
	/*
	*/

	 void exec() ;

        /*
           */
         bool lookup(const QString & key);

private:
        class Private;
        Private * const d ;

        /*
	QString name;
	QString iconPath;
	QString iconName;
        */
};

#endif

